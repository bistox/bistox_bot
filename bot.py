import concurrent.futures
import asyncio
from uuid import uuid4
import random
import time
import sys

import api_pb2_grpc as api
import api_pb2 as proto
import common_pb2 as common


CURRENCY_MAP = {
    common.BTC: 'BTC',
    common.ETH: 'ETH',
    common.USD: 'USD',
    common.DASH: 'DASH',
    common.BCH: 'BCH',
    common.LTC: 'LTC'
}

MAP = {
    'order_type': {'MARKET': common.MARKET, 'LIMIT': common.LIMIT},
    'operation_type': {'BUY': common.BUY, 'SELL': common.SELL},
    'currency': {
        'BTC': common.BTC,
        'ETH': common.ETH,
        'USD': common.USD,
        'LTC': common.LTC,
        'BCH': common.BCH,
        'DASH': common.DASH
    },
}

PAIRS = {
    'ETH/BTC': 0.034,
    'DASH/BTC': 0.0237,
    'LTC/BTC': 0.014,
    'BCH/BTC': 0.033,
    'BTC/USD': 385338,
    'LTC/USD': 5570,
    'BCH/USD': 12723,
    'ETH/USD': 13130,
    'DASH/USD': 9333
}

executor = concurrent.futures.ThreadPoolExecutor(max_workers=4)


coefs = {
    'wave': random.random(),
    'dir': random.choice([-1, 1, 2])
}


async def change_coefs():
    while True:
        await asyncio.sleep(60 * 5)
        coefs['wave'] = random.random()
        coefs['dir'] = random.choice([1, -1])


class TradeBot:
    def __init__(self, id, host):
        self.id = id
        ch = api.grpc.insecure_channel(host)
        stub = api.BistoxApiStub(ch)
        self.stub = stub
        self.login = uuid4().hex
        self._sign_up(self.login, 'pass')

    def async_session(fn):
        async def wrapper(*args, **kwargs):
            self = args[0]
            method, req = fn(*args, **kwargs)
            fut = getattr(method, 'future')(request=req, metadata=self.meta)
            fut = executor.submit(fut.result)
            try:
                return await asyncio.wrap_future(fut)
            except Exception as ex:
                pass#print(ex, args, kwargs)
        return wrapper

    def _sign_up(self, login, password):
        req = proto.SignUpRequest(login=login, password=password, email=login)
        self.meta = [('authorization', 'basic ' + self.stub.SignUp(req).token)]

    @async_session
    def wallets(self):
        return self.stub.GetWallets, proto.GetWalletsRequest()

    @async_session
    def wallet(self, currency):
        wallet_id = self.wallets[currency].wallet_id
        return self.stub.GetWallet, proto.GetWalletRequest(wallet_id=wallet_id)

    @async_session
    def fiat_payin(self, amount):
        wallet_id = self.wallets['USD'].wallet_id
        return self.stub.FiatPayin, \
            proto.FiatPayinRequest(wallet_id=wallet_id, amount=amount)

    @async_session
    def buy_crypto(self, crypto_currency, amount):
        data = {
            'wallet_id': self.wallets['USD'].wallet_id,
            'to_wallet_id': self.wallets[crypto_currency].wallet_id,
            'amount': amount
        }
        return self.stub.BuyCrypto, \
            proto.BuyCryptoRequest(**data)

    @async_session
    def get_orders(self, order_type, operation_type, base_currency, quoted_currency):
        data = {
            'order_type': order_type,
            'operation_type': operation_type,
            'base_currency': base_currency,
            'quoted_currency': quoted_currency,
            'deleted': False
        }
        return self.stub.GetOrders, \
            proto.GetOrdersRequest(**data)

    @async_session
    def order(self, order_type, operation_type, base_currency, quoted_currency,
              amount, price, base_wallet_id, quoted_wallet_id):
        '''
        proto.OrderType order_type  = 1;
        proto.OperationType operation_type = 2;
        proto.Currency base_currency = 3;
        proto.Currency quoted_currency = 4;
        int64 amount = 5;
        int64 price = 6;
        string base_wallet_id = 7;
        string quoted_wallet_id = 8;
        '''
        data = {
            'order_type': MAP['order_type'][order_type],
            'operation_type': MAP['operation_type'][operation_type],
            'base_currency': MAP['currency'][base_currency],
            'quoted_currency': MAP['currency'][quoted_currency],
            'amount': amount,
            'price': price,
            'base_wallet_id': base_wallet_id,
            'quoted_wallet_id': quoted_wallet_id
        }
        return self.stub.CreateOrder, proto.CreateOrderRequest(**data)

    async def run(self, default_rate, operation_type, base_currency, quoted_currency, order_type):
        inv_operation_type = 'BUY' if operation_type == 'SELL' else 'BUY'

        wallets = (await self.wallets()).wallets
        self.wallets = {CURRENCY_MAP[w.currency]: w for w in wallets}

        await self.fiat_payin(1000000000)
        quoted_wallet_id = self.wallets[quoted_currency].wallet_id
        base_wallet_id = self.wallets[base_currency].wallet_id
        print(quoted_currency, quoted_wallet_id, base_currency, base_wallet_id, flush=True)

        # Hold balance!

        wave_coef = random.random(), random.random()
        i = 0

        while True:
            await asyncio.sleep(1)
            main_currency = base_currency if operation_type == 'SELL' else quoted_currency
            wallet = await self.wallet(main_currency)
            if wallet.balance < 0.00001:
                await self.buy_crypto(main_currency, 10)
                continue
            #print(self.id, base_currency, wallet.balance)
            orders = await self.get_orders('LIMIT', inv_operation_type, base_currency, quoted_currency)
            orders_inv = await self.get_orders('LIMIT', operation_type, base_currency, quoted_currency)
            orders = orders.orders
            orders_inv = orders_inv.orders

            if len(orders) > 0 and len(orders_inv) > 0 and i % 100 == 0:
                default_rate = (orders[0].price + orders_inv[0].price) / 2
                print(self.id, 'default_rate:', default_rate, flush=True)

            amount = wallet.balance * random.random() * 0.25
            price = 0
            if operation_type == 'SELL':
                price = default_rate + (random.normalvariate(1, 1)) * 0.01
            if operation_type == 'BUY':
                price = default_rate - (random.normalvariate(1, 1)) * 0.01

            price += 0.01 * coefs['dir'] * price * coefs['wave']

            if price <= 0:
                continue

            args = [order_type, operation_type, base_currency, quoted_currency,
                amount, price, base_wallet_id, quoted_wallet_id
            ]
            order = await self.order(*args)
            i += 1


if __name__ == '__main__':
    pair = sys.argv[1]
    base_currency, quoted_currency = pair.split('/')
    default_rate = PAIRS[pair]

    loop = asyncio.get_event_loop()
    for i in range(10):
        operation_type = ['SELL', 'BUY'][i % 2]
        args = [default_rate, operation_type, base_currency, quoted_currency, 'LIMIT']
        bot = TradeBot(i, '68.183.240.93:8888').run(*args)
        loop.create_task(bot)
    for i in range(2):
        operation_type = ['SELL', 'BUY'][i % 2]
        args = [default_rate, operation_type, base_currency, quoted_currency, 'MARKET']
        bot = TradeBot(i, '68.183.240.93:8888').run(*args)
        loop.create_task(bot)
    loop.create_task(change_coefs())
    loop.run_forever()
